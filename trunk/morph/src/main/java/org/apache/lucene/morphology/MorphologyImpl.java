/**
 * Copyright 2009 Alexander Kuznetsov
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.apache.lucene.morphology;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MorphologyImpl implements Morphology {

    protected Heuristic[][] rules;
    protected String[] grammarInfo;
    protected LetterDecoderEncoder decoderEncoder;
    protected Boolean isRussian;

    protected ArrayList<int[]> intsList = new ArrayList<int[]>();
    protected ArrayList<Short> rulesIdList = new ArrayList<Short>();

    protected ArrayList<int[]> intsListWithDefis = new ArrayList<int[]>();
    protected ArrayList<Short> rulesIdListWithDefis = new ArrayList<Short>();

    public MorphologyImpl(String fileName, LetterDecoderEncoder decoderEncoder, Boolean isRussian) throws IOException {
        readFromFile(fileName);
        this.decoderEncoder = decoderEncoder;
        this.isRussian = isRussian;
    }

    public MorphologyImpl(InputStream inputStream, LetterDecoderEncoder decoderEncoder, Boolean isRussian) throws IOException {
        readFromInputStream(inputStream);
        this.decoderEncoder = decoderEncoder;
        this.isRussian = isRussian;
    }

    public MorphologyImpl(ArrayList<int[]> intsList, ArrayList<Short> rulesIdList, Heuristic[][] rules, String[] grammarInfo, Boolean isRussian, ArrayList<int[]> intsListWithDefis, ArrayList<Short> rulesIdListWithDefis) {
        this.rules = rules;
        this.grammarInfo = grammarInfo;
        this.isRussian = isRussian;

        this.intsList = intsList;
        this.rulesIdList = rulesIdList;

        this.intsListWithDefis = intsListWithDefis;
        this.rulesIdListWithDefis = rulesIdListWithDefis;
    }

    public List<String> getNormalForms(String s) {
        ArrayList<String> result = new ArrayList<String>();
        int[] ints;
        if (isRussian) {
            ints = decoderEncoder.encodeToArray(s);
        } else {
            ints = decoderEncoder.encodeToArray(revertWord(s));
        }
        int ruleId;

        if (s.contains("-")) {
            ruleId = findRuleId(ints, true);
            for (Heuristic h : rules[rulesIdListWithDefis.get(ruleId)]) {
                result.add(h.transformWord(s).toString());
            }
        } else {
            ruleId = findRuleId(ints, false);
            for (Heuristic h : rules[rulesIdList.get(ruleId)]) {
                result.add(h.transformWord(s).toString());
            }
        }

        return result;
    }

    public List<String> getMorphInfo(String s) {
        ArrayList<String> result = new ArrayList<String>();
        int[] ints;
        if (isRussian) {
            ints = decoderEncoder.encodeToArray(s);
        } else {
            ints = decoderEncoder.encodeToArray(revertWord(s));
        }

        int ruleId;
        if (s.contains("-")) {
            ruleId = findRuleId(ints, true);
            for (Heuristic h : rules[rulesIdListWithDefis.get(ruleId)]) {
                result.add(h.transformWord(s).append("|").append(grammarInfo[h.getFormMorphInfo()]).toString());
            }
        } else {
            ruleId = findRuleId(ints, false);
            for (Heuristic h : rules[rulesIdList.get(ruleId)]) {
                result.add(h.transformWord(s).append("|").append(grammarInfo[h.getFormMorphInfo()]).toString());
            }
        }

        return result;
    }

    protected int findRuleId(int[] ints, Boolean withTire) {
        int mid = 0;

        if (!withTire) {
            int low = 0;
            int high = intsList.size() - 1;

            while (low <= high) {
                mid = (low + high) >>> 1;
                int[] midVal = intsList.get(mid);

                int comResult = compareToInts(ints, midVal);
                if (comResult > 0) {
                    low = mid + 1;
                } else if (comResult < 0) {
                    high = mid - 1;
                } else {
                    break;
                }
            }
            if (compareToInts(ints, intsList.get(mid)) >= 0) {
//            return mid;
            } else {
                mid = mid - 1;
//            return mid - 1;
            }
        } else {
            int low = 0;
            int high = intsListWithDefis.size() - 1;

            while (low <= high) {
                mid = (low + high) >>> 1;
                int[] midVal = intsListWithDefis.get(mid);

                int comResult = compareToInts(ints, midVal);
                if (comResult > 0) {
                    low = mid + 1;
                } else if (comResult < 0) {
                    high = mid - 1;
                } else {
                    System.out.println("!!!!!!");
                    break;
                }
            }

            int[] extractedArr = intsListWithDefis.get(mid);

            if (compareToInts(ints, extractedArr) == 0) {
                System.out.println("++++");
            } else if (compareToInts(ints, extractedArr) >= 0) {
//            return mid;
                System.out.println("++++");
            } else {
                mid = mid - 1;
//            return mid - 1;
            }
        }

        return mid;

    }

    private int compareToInts(int[] i1, int[] i2) {
        int minLength = Math.min(i1.length, i2.length);
        for (int i = 0; i < minLength; i++) {
            int i3;

            if (i1[i] < i2[i]) {
                i3 = -1;
            } else {
                if (i1[i] == i2[i]) {
                    i3 = 0;
                } else {
                    i3 = 1;
                }
            }
            if (i3 != 0) {
                return i3;
            }
        }
        return i1.length - i2.length;
    }

    public void writeToFile(String fileName) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8");
        writer.write(intsList.size() + "\n");
        for (int[] i : intsList) {
            writer.write(i.length + "\n");
            for (int j : i) {
                writer.write(j + "\n");
            }
        }
        for (short i : rulesIdList) {
            writer.write(i + "\n");
        }
        writer.write(rules.length + "\n");
        for (Heuristic[] heuristics : rules) {
            writer.write(heuristics.length + "\n");
            for (Heuristic heuristic : heuristics) {
                writer.write(heuristic.toString() + "\n");
            }
        }
        writer.write(grammarInfo.length + "\n");
        for (String s : grammarInfo) {
            writer.write(s + "\n");
        }

        if (intsListWithDefis.size() > 0) {
            writer.write(intsListWithDefis.size() + "\n");
            for (int[] i : intsListWithDefis) {
                writer.write(i.length + "\n");
                for (int j : i) {
                    writer.write(j + "\n");
                }
            }

            for (short i : rulesIdListWithDefis) {
                writer.write(i + "\n");
            }
        } else {
            writer.write(intsListWithDefis.size() + "\n");
        }

        writer.close();
    }

    public void writeToFile_OLD(String fileName) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8");
        writer.write(intsList.size() + "\n");
        for (int[] i : intsList) {
            writer.write(i.length + "\n");
            for (int j : i) {
                writer.write(j + "\n");
            }
        }
        for (short i : rulesIdList) {
            writer.write(i + "\n");
        }
        writer.write(rules.length + "\n");
        for (Heuristic[] heuristics : rules) {
            writer.write(heuristics.length + "\n");
            for (Heuristic heuristic : heuristics) {
                writer.write(heuristic.toString() + "\n");
            }
        }
        writer.write(grammarInfo.length + "\n");
        for (String s : grammarInfo) {
            writer.write(s + "\n");
        }
        writer.close();
    }

    public void readFromFile(String fileName) throws IOException {
        FileInputStream inputStream = new FileInputStream(fileName);
        readFromInputStream(inputStream);
    }

    private void readFromInputStream(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        String s = bufferedReader.readLine();
        Integer amount = Integer.valueOf(s);

        readSeparators(bufferedReader, amount);
        readRulesId(bufferedReader, amount);

        readRules(bufferedReader);
        readGrammaInfo(bufferedReader);

        String a_s = bufferedReader.readLine();
        Integer a_amount = Integer.valueOf(a_s);

        a_readSeparators(bufferedReader, a_amount);
        a_readRulesId(bufferedReader, a_amount);

        bufferedReader.close();
    }

    private void a_readRulesId(BufferedReader bufferedReader, Integer amount) throws IOException {
        rulesIdListWithDefis = new ArrayList<Short>();
        for (int i = 0; i < amount; i++) {
            String s1 = bufferedReader.readLine();
            rulesIdListWithDefis.add(Short.valueOf(s1));
        }
    }

    private void a_readSeparators(BufferedReader bufferedReader, Integer amount) throws IOException {
        intsListWithDefis = new ArrayList<int[]>();
        for (int i = 0; i < amount; i++) {
            String s1 = bufferedReader.readLine();
            Integer wordLenght = Integer.valueOf(s1);
            int[] tmpIntArr = new int[wordLenght];
            for (int j = 0; j < wordLenght; j++) {
                tmpIntArr[j] = Integer.valueOf(bufferedReader.readLine());
            }
            intsListWithDefis.add(tmpIntArr);
        }
    }

    private void readGrammaInfo(BufferedReader bufferedReader) throws IOException {
        String s;
        Integer amount;
        s = bufferedReader.readLine();
        amount = Integer.valueOf(s);
        grammarInfo = new String[amount];
        for (int i = 0; i < amount; i++) {
            grammarInfo[i] = bufferedReader.readLine();
        }
    }

    protected void readRules(BufferedReader bufferedReader) throws IOException {
        String s;
        Integer amount;
        s = bufferedReader.readLine();
        amount = Integer.valueOf(s);
        rules = new Heuristic[amount][];
        for (int i = 0; i < amount; i++) {
            String s1 = bufferedReader.readLine();
            Integer ruleLength = Integer.valueOf(s1);
            rules[i] = new Heuristic[ruleLength];
            for (int j = 0; j < ruleLength; j++) {
                rules[i][j] = new Heuristic(bufferedReader.readLine());
            }
        }
    }

    private void readRulesId(BufferedReader bufferedReader, Integer amount) throws IOException {
        rulesIdList = new ArrayList<Short>();
        for (int i = 0; i < amount; i++) {
            String s1 = bufferedReader.readLine();
            rulesIdList.add(Short.valueOf(s1));
        }
    }

    private void readSeparators(BufferedReader bufferedReader, Integer amount) throws IOException {
        intsList = new ArrayList<int[]>();
        for (int i = 0; i < amount; i++) {
            String s1 = bufferedReader.readLine();
            Integer wordLenght = Integer.valueOf(s1);
            int[] tmpIntArr = new int[wordLenght];
            for (int j = 0; j < wordLenght; j++) {
                tmpIntArr[j] = Integer.valueOf(bufferedReader.readLine());
//                separators[i][j] = Integer.valueOf(bufferedReader.readLine());
            }
            intsList.add(tmpIntArr);
        }
    }

    protected String revertWord(String s) {
        StringBuilder result = new StringBuilder();
        for (int i = 1; i <= s.length(); i++) {
            result.append(s.charAt(s.length() - i));
        }
        return result.toString();
    }
}
