/**
 * Copyright 2009 Alexander Kuznetsov
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.mycompany.checkrm;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.morphology.LuceneMorphology;
import org.apache.lucene.morphology.english.EnglishLuceneMorphology;
import org.apache.lucene.morphology.russian.RussianLuceneMorphology;

/**
 *
 * @author wwwdev
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<String> wordBaseForms;
        try {
            LuceneMorphology luceneMorph = new RussianLuceneMorphology();
//            String str="когда-то";
//            System.out.println("" +( 100 % 34));
//            System.out.println(0 + "-".charAt(0));
//            System.out.println(0+"ь".charAt(0));
//            System.out.println((0+str.charAt(5)));
//            System.out.println((0+str.charAt(7)));
//            System.out.println(Character.toString((char)33));
//            System.out.println(Character.toString((char)1103));
//            System.out.println(Character.toString((char)1104));
//            wordBaseForms = luceneMorph.getMorphInfo("ерза");
//            System.out.println(wordBaseForms);
//            wordBaseForms = luceneMorph.getMorphInfo("абудабийкою");
//            System.out.println(wordBaseForms);
//            wordBaseForms = luceneMorph.getMorphInfo("абудабийка");
//            System.out.println(wordBaseForms);
//            wordBaseForms = luceneMorph.getMorphInfo("ежистей");
//            System.out.println(wordBaseForms);
//             System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
//            wordBaseForms = luceneMorph.getMorphInfo("абдельазиз");
//            System.out.println(wordBaseForms);
//             System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
//            wordBaseForms = luceneMorph.getMorphInfo("абак");
//            System.out.println(wordBaseForms);
//              System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
//            wordBaseForms = luceneMorph.getMorphInfo("ерзала");
//            System.out.println(wordBaseForms);
//            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
//            wordBaseForms = luceneMorph.getMorphInfo("ерзало");
//            System.out.println(wordBaseForms);
//            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
//            wordBaseForms = luceneMorph.getMorphInfo("ерзает");
//            System.out.println(wordBaseForms);
//            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
////            wordBaseForms = luceneMorph.getMorphInfo("либо");
////            System.out.println(wordBaseForms);
//            wordBaseForms = luceneMorph.getMorphInfo("откудова");
//            System.out.println(wordBaseForms);
//            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
            wordBaseForms = luceneMorph.getMorphInfo("ёжик");
            System.out.println(wordBaseForms);
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
            wordBaseForms = luceneMorph.getMorphInfo("когда-либо");
            System.out.println(wordBaseForms);
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
            wordBaseForms = luceneMorph.getMorphInfo("когда-нибудь");
            System.out.println(wordBaseForms);
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
            wordBaseForms = luceneMorph.getMorphInfo("когда-то");
            System.out.println(wordBaseForms);
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");

            LuceneMorphology luceneMorphEng = new EnglishLuceneMorphology();
            wordBaseForms = luceneMorphEng.getMorphInfo("non-self-governing");
            System.out.println(wordBaseForms);
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
            wordBaseForms = luceneMorphEng.getMorphInfo("self-destruction");
            System.out.println(wordBaseForms);
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
            wordBaseForms = luceneMorphEng.getMorphInfo("wifi");
            System.out.println(wordBaseForms);
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
        } catch (IOException ex) {
            Logger.getLogger(NewMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
